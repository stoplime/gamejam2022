using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraControl : MonoBehaviour
{
    public float MoveSpeed = 3000;
    public float RotSpeed = 300;

    private PlayerInput playerInput;
    private InputAction cameraMove;
    private InputAction cameraRotation;
    private InputAction cameraZoom;

    private Rigidbody rb;

    private void Awake() {
        playerInput = GetComponent<PlayerInput>();
        cameraMove = playerInput.actions["Move"];
        cameraRotation = playerInput.actions["Rotation"];
        cameraZoom = playerInput.actions["Zoom"];

        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate() {
        Vector2 movement = cameraMove.ReadValue<Vector2>();
        Vector3 vector = new Vector3(movement.x, 0.0f, movement.y);
        rb.AddForce(vector * MoveSpeed * Time.deltaTime);

        Vector2 rotation = cameraRotation.ReadValue<Vector2>();
        // calculate horizontal axis (x axis) in global ref
        // Vector3 localX = transform.rotation * Vector3.right;
        Vector3 localX = Vector3.zero;
        Vector3 rotVector = new Vector3(localX.y, rotation.x, localX.z);
        transform.eulerAngles += rotVector * RotSpeed * Time.deltaTime;

        // Debug.Log(vector);
        Debug.Log(rotVector);
    }
}
